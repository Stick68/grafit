import openpyxl
from openpyxl import load_workbook
import tkinter as tk
import tkinter as ttk
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

fichier_excel = load_workbook("base_donnees.xlsx")


def questionnaire_debut(): #QUESTIONNAIRE DU DEBUT POUR LES QUESTIONS D'INSCRIPTION
    
    global fenetre_questionnaire_debut
    fenetre_page_accueil.destroy()
    
    def premier_enregistrement():
        
        #Prérequis Openpyxl
        wb = fichier_excel
        feuille_principale = wb.worksheets[0]

        #Assignation de l'ID + se placer sur la bonne ligne dans la fenètre principale
        nombre_fenetre = 0
        for sheet in wb:
            nombre_fenetre += 1
        ligne = nombre_fenetre +1
        id = nombre_fenetre
        
        #Rentrer les données du formulaire dans la base de données
        feuille_principale.cell(ligne, 1).value = id
        feuille_principale.cell(ligne, 2).value = entry_nom.get()
        feuille_principale.cell(ligne, 3).value = entry_prenom.get()
        feuille_principale.cell(ligne, 4).value = entry_mail.get()
        feuille_principale.cell(ligne, 5).value = entry_age.get()
        feuille_principale.cell(ligne, 6).value = entry_taille.get()
        feuille_principale.cell(ligne, 7).value = entry_poids.get()

        #Création de la nouvelle feuille hebdomadaire
        nouvelle_feuille = wb.create_sheet()
        nouvelle_feuille.title = str(id)
        nouvelle_feuille.cell(1, 1).value = 'Semaine'
        nouvelle_feuille.cell(1,2).value = 'Poids'
        nouvelle_feuille.cell(1,3).value = 'Activité sportive'
        nouvelle_feuille.cell(1,4).value = 'Sommeil'
        nouvelle_feuille.cell(1, 5).value = 0
        nouvelle_feuille.cell(1, 6).value = id
        
        # Enregistrer les modifications dans le fichier Excel
        wb.save("base_donnees.xlsx")
        formulaire(id)     
    
    #Création de la fenêtre
    fenetre_questionnaire_debut = tk.Tk()
    fenetre_questionnaire_debut.title("Questionnaire de départ")
    
    #Création des questions
    label_titre = tk.Label(fenetre_questionnaire_debut, text="Bienvenue, merci de rentrez vos informations pour créer un compte.")
    label_titre.grid(row=1, column=0,padx=10, pady=5)
    
    #Question - Nom
    label_nom = tk.Label(fenetre_questionnaire_debut, text="Entrez votre nom") #Créer la question
    label_nom.grid(row=2, column=0, sticky="w", padx=10, pady=5) #Design de la question
    entry_nom = tk.Entry(fenetre_questionnaire_debut) #Créer l'espace texte
    entry_nom.grid(row=2,column=1, padx=10, pady=5) #Design de l'espace texte
    
    #Question - Prénom
    label_prenom = tk.Label(fenetre_questionnaire_debut, text="Entrez votre prénom") #Créer la question
    label_prenom.grid(row=3, column=0, sticky="w", padx=10, pady=5) #Design de la question
    entry_prenom = tk.Entry(fenetre_questionnaire_debut) #Créer l'espace texte
    entry_prenom.grid(row=3,column=1, padx=10, pady=5) #Design de l'espace texte
    
    #Question - MAIL
    label_mail = tk.Label(fenetre_questionnaire_debut, text="Entrez votre mail")
    label_mail.grid(row=4, column=0, sticky="w", padx=10, pady=5)
    entry_mail = tk.Entry(fenetre_questionnaire_debut)
    entry_mail.grid(row=4,column=1, padx=10, pady=5)
    
    #Question - âge
    label_age = tk.Label(fenetre_questionnaire_debut, text="Entrez votre âge")
    label_age.grid(row=5, column=0, sticky="w", padx=10, pady=5)
    entry_age = tk.Entry(fenetre_questionnaire_debut)
    entry_age.grid(row=5,column=1, padx=10, pady=5)
    
    #Question - Taille
    label_taille = tk.Label(fenetre_questionnaire_debut, text="Entrez votre taille (en cm)")
    label_taille.grid(row=6, column=0, sticky="w", padx=10, pady=5)
    entry_taille = tk.Entry(fenetre_questionnaire_debut)
    entry_taille.grid(row=6,column=1, padx=10, pady=5)

    #Question - poids
    label_poids = tk.Label(fenetre_questionnaire_debut, text="Entrez votre poids (en kg)")
    label_poids.grid(row=7, column=0, sticky="w", padx=10, pady=5)
    entry_poids = tk.Entry(fenetre_questionnaire_debut)
    entry_poids.grid(row=7,column=1, padx=10, pady=5)
    
    def bouton_retour():
        fenetre_questionnaire_debut.destroy()
        page_accueil()
    #Bouton - Retour
    bouton_fin_retour = tk.Button(fenetre_questionnaire_debut,text="Retour", command=bouton_retour,)
    bouton_fin_retour.grid(row=10, column=0,padx=10, pady=5)

    
    #Bouton - Terminé
    bouton_fin_entrer = tk.Button(fenetre_questionnaire_debut,text="Terminé",command=premier_enregistrement)
    bouton_fin_entrer.grid(row=10, column=1,padx=10, pady=5)

    fenetre_page_accueil.destroy()   
    fenetre_questionnaire_debut.mainloop()

def page_accueil(): #Page Accueil - Permet de dire si on possède déjà un profil 
    
    global fenetre_page_accueil

    
    #Création de la fenêtre
    fenetre_page_accueil = tk.Tk()
    fenetre_page_accueil.title("Page d'accueil")
    
    #Label
    label_question_sur_compte = tk.Label(fenetre_page_accueil, text="Avez-vous déjà un profil ?")
    label_question_sur_compte.grid(row=1, column=0, padx=10, pady=5)
    
    #Bouton OUI
    bouton_compte_oui = tk.Button(fenetre_page_accueil, text="OUI",command=page_profil)
    bouton_compte_oui.grid(row=2, column=0, padx=5, pady=2.5)

    #Bouton NON
    bouton_compte_non = tk.Button(fenetre_page_accueil, text="NON", command=questionnaire_debut)
    bouton_compte_non.grid(row=2, column=1, padx=5, pady=2.5)
    
    fenetre_page_accueil.mainloop()

def page_profil(): #PERMET DE SELECTIONNER LE PROFIL 
    
    global fenetre_profils
    fenetre_page_accueil.destroy()
    
    fenetre_profils = tk.Tk()
    fenetre_profils.title("Profils")

    wb = fichier_excel
    feuille_principale = wb.worksheets[0]
    i = 1
    for sheet in wb:
        i+=1            
        if str(feuille_principale.cell(i,3).value) != "None":
            label_titre = tk.Button(fenetre_profils, text=str(feuille_principale.cell(i,3).value), command=lambda id=i-1: lecture(id))
            label_titre.grid(row=1, column=i,padx=10, pady=5)
    
    fenetre_profils.mainloop()

    
def formulaire(id): #PAGE QUI PERMET D'ENREGISTRER LES NOUVELLES INFORMATIONS DE LA SEMAINE
    
    global fenetre_formulaire
    
    def enregistrement_formulaire():
        wb = fichier_excel
        feuille = wb[str(id)]
        print(feuille['F1'].value)
        feuille['E1'] = feuille['E1'].value + 1
        nombre_semaine = feuille['E1'].value
        
        feuille.cell(nombre_semaine+1, 1).value = nombre_semaine
        feuille.cell(nombre_semaine+1, 2).value = int(entry_poids.get())
        feuille.cell(nombre_semaine+1, 3).value = int(entry_sport.get())
        feuille.cell(nombre_semaine+1, 4).value = int(entry_sommeil.get())
        
        wb.save("base_donnees.xlsx")
        lecture(feuille['F1'].value)
        
    def action_bouton_retour():
        wb = fichier_excel
        feuille = wb.active
        lecture(feuille['F1'].value)
    
    
    fenetre_formulaire = tk.Tk()
    fenetre_formulaire.title("Questionnaire de départ")
        
    label_titre = tk.Label(fenetre_formulaire, text="Merci de rentrez vos nouvelles informations de la semaine !")
    label_titre.grid(row=1, column=0,padx=10, pady=5)
        
    label_sommeil = tk.Label(fenetre_formulaire, text="Entrez votre temps de sommeil") #Créer la question
    label_sommeil.grid(row=2, column=0, sticky="w", padx=10, pady=5) #Design de la question
    entry_sommeil = tk.Entry(fenetre_formulaire) #Créer l'espace texte
    entry_sommeil.grid(row=2,column=1, padx=10, pady=5) #Design de l'espace texte
        
    label_poids = tk.Label(fenetre_formulaire, text="Entrez votre nouveaux poids") #Créer la question
    label_poids.grid(row=3, column=0, sticky="w", padx=10, pady=5) #Design de la question
    entry_poids = tk.Entry(fenetre_formulaire) #Créer l'espace texte
    entry_poids.grid(row=3,column=1, padx=10, pady=5) #Design de l'espace texte
        
    label_sport = tk.Label(fenetre_formulaire, text="Entrez votre temps de sport") #Créer la question
    label_sport.grid(row=4, column=0, sticky="w", padx=10, pady=5) #Design de la question
    entry_sport = tk.Entry(fenetre_formulaire) #Créer l'espace texte
    entry_sport.grid(row=4,column=1, padx=10, pady=5) #Design de l'espace texte

    bouton_fin_retour = tk.Button(fenetre_formulaire,text="Retour",command=action_bouton_retour)
    bouton_fin_retour.grid(row=10, column=0,padx=10, pady=5)
        
    bouton_fin_entrer = tk.Button(fenetre_formulaire,text="Terminé",command=enregistrement_formulaire)
    bouton_fin_entrer.grid(row=10, column=1,padx=10, pady=5)
        
    fenetre_formulaire.mainloop()

# Création d'une fonction qui lie les données du fichier et les rentre chacun dans la liste correspondante
def lecture(id):
    
    workbook = fichier_excel
    
    
    liste_semaine = []
    liste_poids = []
    liste_sport = []
    liste_sommeil = []
    sheet = workbook[str(id)]
    i = 1
    for a in range(sheet['E1'].value):
        i = i+1
        liste_semaine.append(sheet.cell(i, 1).value)
        liste_poids.append(sheet.cell(i, 2).value)
        liste_sport.append(sheet.cell(i, 3).value)
        liste_sommeil.append(sheet.cell(i, 4).value)

    # Calcul de la différence de poids
    difference_poids = liste_poids[-1] - liste_poids[0]

    # Calcul de la moyenne de l'activité sportive
    moyenne_sport = sum(liste_sport) / len(liste_sport)

    # Calcul de la moyenne du sommeil
    moyenne_sommeil = sum(liste_sommeil) / len(liste_sommeil)

    # Création d'une fenêtre Tkinter
    fenetre = tk.Tk()
    fenetre.title("Évolution des données")
    fenetre.attributes("-fullscreen", False)

    # Création de trois cadres pour chaque colonne
    cadre_gauche = tk.Frame(fenetre)
    cadre_gauche.pack(side=tk.LEFT, fill=tk.BOTH, )

    cadre_centre = tk.Frame(fenetre)
    cadre_centre.pack(side=tk.LEFT, fill=tk.BOTH,)

    cadre_droite = tk.Frame(fenetre)
    cadre_droite.pack(side=tk.LEFT)

    # Création du premier graphique dans la colonne de gauche
    fig1, ax1 = plt.subplots()
    ax1.plot(liste_semaine, liste_poids)
    ax1.set_title('Évolution du poids')
    ax1.set_xlabel('Semaine')
    ax1.set_ylabel('Poids (kg)')
    affichage1 = FigureCanvasTkAgg(fig1, master=cadre_gauche)
    affichage1.draw()
    affichage1.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=True)

    # Affichage de la différence de poids à côté du premier graphique
    label_diff_poids = tk.Label(cadre_gauche, text=f"Différence de poids: {difference_poids:.2f} kg")
    label_diff_poids.pack(side=tk.TOP)

    # Création du deuxième graphique dans la colonne du centre
    fig2, ax2 = plt.subplots()
    ax2.plot(liste_semaine, liste_sport)
    ax2.set_title('Évolution de l\'activité sportive')
    ax2.set_xlabel('Semaine')
    ax2.set_ylabel('Sport (h)')
    affichage2 = FigureCanvasTkAgg(fig2, master=cadre_centre)
    affichage2.draw()
    affichage2.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=True)

    # Affichage de la moyenne de l'activité sportive à côté du deuxième graphique
    label_moy_sport = tk.Label(cadre_centre, text=f"Moyenne de l'activité sportive: {moyenne_sport:.2f} h")
    label_moy_sport.pack(side=tk.TOP)

    # Création du troisième graphique dans la colonne de droite
    fig3, ax3 = plt.subplots()
    ax3.plot(liste_semaine, liste_sommeil)
    ax3.set_title('Évolution du sommeil')
    ax3.set_xlabel('Semaine')
    ax3.set_ylabel('Sommeil (h)')
    affichage3 = FigureCanvasTkAgg(fig3, master=cadre_droite)
    affichage3.draw()
    affichage3.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=True)

    # Affichage de la moyenne du sommeil à côté du troisième graphique
    label_moy_sommeil = tk.Label(cadre_droite, text=f"Moyenne du sommeil: {moyenne_sommeil:.2f} h")
    label_moy_sommeil.pack(side=tk.TOP)
        
    entry_bouton_formulaire = tk.Button(cadre_droite, command=lambda: formulaire(sheet['F1'].value), text="Nouvelles Données")
    entry_bouton_formulaire.pack(side=tk.LEFT, fill=tk.BOTH)

    # Lancement de la boucle principale de la fenêtre
    fenetre.mainloop() 
    fenetre_profils.destroy()
    


page_accueil()